import "./business/gitmodels"
import { StringUtils } from "./infra/stringUtils"
import express from "express";

class App {
    port = 8080;
    start() {
        const app = express();
        app.listen(this.port,  () => {
            console.log("Server start successfully");
        });

        app.get("/", ( req, res ) => {
            res.send( "Hello world!" );
        }); 
    }
}
// Start App
var app = new App();
app.start();