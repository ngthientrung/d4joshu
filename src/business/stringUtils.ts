export class StringUtils {
    static hasTags(text: string): boolean {
        return text.trim().split('')[0] == '#';
    }
    
    static getAllTags(text: string): Array<string> {
        if(!this.hasTags(text)) {
            return Array();
        }
        var firstLine = text.toLowerCase().trim().split('\n')[0];
        const regex = /#([a-z]*)/gm
        var output = new Array<string>();
        var expArray: RegExpExecArray | null = null;
        while((expArray = regex.exec(firstLine)) != null) {
            expArray.forEach(element => {
                if(!element.startsWith("#")) {
                    output.push(element);
                }
            });
        }

        return output;
    }
}