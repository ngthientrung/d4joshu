
interface BaseLink {
    link: string
}

class GComment implements BaseLink {
    cId: string = "";
    content = "";
    link = "";
    user: GUser | undefined = undefined;
}

class GUser implements BaseLink {
    uId: string;
    name: string;
    avatar: string;
    link = "";

    constructor(uId: string, name: string, avatar: string) {
        this.avatar = avatar;
        this.name = name;
        this.uId = uId;
    }
}

class GPullRequest implements BaseLink {
    prId = "";
    title = "";
    baseBranch = "";
    devBranch = "";
    creator: GUser | undefined = undefined;
    comments: Array<GComment> = Array();
    link = "";
}

class GIssue implements BaseLink {
    iId = "";
    title = "";
    content = "";
    creator: GUser | undefined = undefined;
    comments: Array<GComment> = Array();
    link = "";
}